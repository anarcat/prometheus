#!/usr/bin/make -f
# -*- Mode: makefile -*-
include /usr/share/dpkg/pkg-info.mk

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Include test fixtures.
export DH_GOLANG_INSTALL_EXTRA := \
    cmd/prometheus/testdata \
    cmd/promtool/testdata \
    config/testdata \
    discovery/file/fixtures \
    discovery/http/fixtures \
    discovery/moby/testdata \
    discovery/puppetdb/fixtures \
    documentation/examples/prometheus-agent.yml \
    documentation/examples/prometheus.yml \
    model/rulefmt/testdata \
    promql/fuzz-data \
    promql/testdata \
    rules/fixtures \
    scrape/testdata \
    tsdb/testdata \
    # EOL

# Do not build examples.
export DH_GOLANG_EXCLUDES := documentation

BUILDDIR := $(CURDIR)/.build
METAPKG := github.com/prometheus/common

# Taken and modified from the project's Makefile.

BRANCH     := debian/sid
USER       := team+pkg-go@tracker.debian.org
BUILD_DATE  = $(shell date --utc --date='@$(SOURCE_DATE_EPOCH)' \
    +%Y%m%d-%H:%M:%S)
GO_VERSION  = $(shell go version | sed 's/go version \(\S*\).*/\1/')
BUILDFLAGS  = -ldflags \
  " -X $(METAPKG)/version.Version=$(DEB_VERSION_UPSTREAM)\
    -X $(METAPKG)/version.Revision=$(DEB_VERSION)\
    -X $(METAPKG)/version.Branch=$(BRANCH)\
    -X $(METAPKG)/version.BuildUser=$(USER)\
    -X $(METAPKG)/version.BuildDate=$(BUILD_DATE)\
    -X $(METAPKG)/version.GoVersion=$(GO_VERSION)"

ifeq ($(DEB_HOST_ARCH_CPU),arm)
# Tests in armel take way too long, and run out of memory in armhf.
TESTFLAGS  := -timeout 60m -short
else
TESTFLAGS  := -timeout 20m
endif

%:
	# stop the service before upgrading, so we can move the v1 database away
	dh $@ --buildsystem=golang --builddirectory=$(BUILDDIR)

WHATIS1 := "prometheus \\- The Prometheus monitoring server"
WHATIS2 := "promtool \\- Tooling for the Prometheus monitoring system"
PROMPB := $(BUILDDIR)/src/github.com/prometheus/prometheus/prompb

override_dh_auto_configure:
	dh_auto_configure -- $(BUILDFLAGS)
	protoc --gogofast_out=plugins=grpc:$(PROMPB) \
            -I$(PROMPB) \
            -I$(BUILDDIR)/src/github.com/gogo/protobuf \
            -I$(BUILDDIR)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
            $(PROMPB)/*.proto

# Exclude package without non-test files.
override_dh_auto_build: DH_GOLANG_EXCLUDES += tsdb/test
override_dh_auto_build:
	dh_auto_build -- $(BUILDFLAGS)
	$(BUILDDIR)/bin/prometheus --help-man > $(BUILDDIR)/prometheus.1
	$(BUILDDIR)/bin/promtool --help-man > $(BUILDDIR)/promtool.1
	# Fix whatis entry.
	sed -i '/^.SH "NAME"/,+1c.SH "NAME"\n'$(WHATIS1) $(BUILDDIR)/prometheus.1
	sed -i '/^.SH "NAME"/,+1c.SH "NAME"\n'$(WHATIS2) $(BUILDDIR)/promtool.1

# Avoid test failures due to newer crypto/x509 requirements until upstream
# updates the test CA certificate.
override_dh_auto_test: export GODEBUG := x509sha1=1
override_dh_auto_test:
	dh_auto_test -- $(TESTFLAGS)

override_dh_auto_install:
	dh_auto_install -- --no-source
